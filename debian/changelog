oscar (1.5.3-2) unstable; urgency=medium

  * Team upload.
  * Fix clean target
    Closes: #1045364
  * d/copyright: review thanks to lrc

 -- Andreas Tille <tille@debian.org>  Sat, 21 Dec 2024 21:57:51 +0100

oscar (1.5.3-1) unstable; urgency=medium

  * d/watch: Adjust file for new gitlab URLs.
  * New upstream version 1.5.3
  * d/control: Bump Standards-Version to 4.7.0; no changes needed.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 14 Jul 2024 00:17:08 -0400

oscar (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1
  * d/control: Bump Standards-Version to 4.6.2; no changes needed.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 19 Nov 2023 00:44:47 -0500

oscar (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * d/control: Bump Standards-Version to 4.6.1.
  * d/s/lintian-overrides: Silence erroneous source-is-missing.
  * d/p/0001-Create-install-rule-on-Makefile.patch: Add DEP-3 headers.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 20 Jul 2022 13:19:55 -0400

oscar (1.3.1-1) unstable; urgency=medium

  * Team upload.
  * Fix watch file to not report alpha versions

 -- Andreas Tille <tille@debian.org>  Thu, 03 Feb 2022 15:32:45 +0100

oscar (1.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version  (Closes: #984280)
  * Standards-Version: 4.6.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * No tab in license text (routine-update)
  * d/copyright: bump year

 -- Étienne Mollier <emollier@debian.org>  Wed, 03 Nov 2021 21:48:22 +0100

oscar (1.2.0-1) unstable; urgency=medium

  * d/watch: Improve uversionmangle regex.
  * New upstream version 1.2.0
  * d/p/0003-treat-deprecation-copy-as-warning.patch: Remove.
    Upstream incorporated a fix that solves the build issue.
  * d/control: Bump Standards-Version to 4.5.1.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 20 Dec 2020 19:40:18 -0500

oscar (1.1.1-2) unstable; urgency=medium

  * Team Upload.
  * Treat deprecated-copy warnings as warnings rather than errors
    (Closes: #967101)

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 31 Aug 2020 19:53:01 +0530

oscar (1.1.1-1) unstable; urgency=medium

  * d/watch: Add uversionmangle to account for "-rc" suffix.
  * New upstream version 1.1.1. (Closes: #964646)
  * Adjust our local patches for the new release.
    - d/p/0001-Create-install-rule-on-Makefile.patch: Refresh.
    - d/p/0002-Don-t-print-build-date.patch: New patch, derived from...
    - d/p/0002-Remove-__DATE__-and-__TIME__-from-version-string.patch:
      ... this.
    - d/p/0003-Fix-GCC9-implicitly-declared-copy-constructor-is-dep.patch:
      Remove; fixed upstream.
    - d/p/0004-Fix-typos.patch: Remove.  This patch is too big to maintain
      for a small benefit.  I will rework and submit it upstream later.
  * d/rules: Adjust configure step to use OSCAR_QT.pro explicitly.
  * d/control: Bump Standards-Version to 4.5.
  * d/control: Bump debhelper-compat to 13.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Thu, 09 Jul 2020 17:16:26 -0400

oscar (1.1.0-testing-3-3) unstable; urgency=medium

  * Add "Provides: sleepyhead" to d/control.
    OSCAR is a fork of Sleepyhead, which has been abandoned by the
    upstream maintainer.  For this reason, and in order to keep Sleepyhead
    users up-to-date, we tell apt that OSCAR "Provides: sleepyhead".

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sat, 18 Jan 2020 11:50:34 -0500

oscar (1.1.0-testing-3-2) unstable; urgency=low

  * Bump version due to mistake uploading a binary.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 15 Jan 2020 19:24:33 -0500

oscar (1.1.0-testing-3-1) unstable; urgency=medium

  * Initial release (Closes: #940860).

 -- Sergio Durigan Junior <sergiodj@debian.org>  Fri, 20 Sep 2019 20:06:51 -0400
